//
//  CarCell.swift
//  Cars
//
//  Created by Antonio Alves on 5/3/16.
//  Copyright © 2016 Antonio Alves. All rights reserved.
//

import UIKit

class CarCell: UITableViewCell {

    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var carModel: UILabel!
    @IBOutlet weak var carManufacturer: UILabel!
    
    
}
