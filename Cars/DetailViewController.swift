//
//  DetailViewController.swift
//  Cars
//
//  Created by Antonio Alves on 5/3/16.
//  Copyright © 2016 Antonio Alves. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var imageDetailCar: UIImageView!
    @IBOutlet weak var modelName: UILabel!
    @IBOutlet weak var manufaturerName: UILabel!
    @IBOutlet weak var engine: UILabel!
    @IBOutlet weak var speed: UILabel!
    @IBOutlet weak var acceleration: UILabel!
    
    var car: Car?
    override func viewDidLoad() {
        super.viewDidLoad()
        print(car?.imageName)
        if car != nil {
            updateUI()
        }
        // Do any additional setup after loading the view.
    }
    
    private func updateUI() {
        imageDetailCar.image = UIImage(named: car!.imageName)
        modelName.text = car?.model
        manufaturerName.text = car?.manufacturer
        engine.text = car!.engine
        speed.text = "\(car!.speed)"
        acceleration.text = "\(car!.acceleration) seconds"
    }


}
