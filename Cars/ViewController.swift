//
//  ViewController.swift
//  Cars
//
//  Created by Antonio Alves on 5/3/16.
//  Copyright © 2016 Antonio Alves. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var cars = [Car]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        cars = GetCars.getCarsFromFile()
        self.automaticallyAdjustsScrollViewInsets = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            let controller = segue.destinationViewController as! DetailViewController
            if let index = sender as? NSIndexPath {
                let car = cars[index.row]
                controller.car = car
            }
        }
    }

}

extension ViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cars.count > 0 {
            return cars.count
        } else {
            return 1
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        let car = cars[indexPath.row]
        cell.imageView?.image = UIImage(named: car.imageName)
        cell.textLabel?.text = car.model
        cell.detailTextLabel?.text = car.manufacturer
        return cell
    }
    
}

extension ViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("showDetail", sender: indexPath)
        tableView.deselectRowAtIndexPath(indexPath , animated: false)
    }
}

