//
//  Cars.swift
//  Cars
//
//  Created by Antonio Alves on 5/3/16.
//  Copyright © 2016 Antonio Alves. All rights reserved.
//

import Foundation


class Car {
    var model: String = ""
    var manufacturer: String = ""
    var imageName: String = ""
    var engine: String = ""
    var speed: Int = 0
    var acceleration: Int = 0
}