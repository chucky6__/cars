//
//  GetCars.swift
//  Cars
//
//  Created by Antonio Alves on 5/3/16.
//  Copyright © 2016 Antonio Alves. All rights reserved.
//

import Foundation


class GetCars {
    
    static func getCarsFromFile() -> [Car] {
        guard let path = NSBundle.mainBundle().pathForResource("cars", ofType: "json") else {
            print("error geting file")
            return []
        }
        guard let data = NSData(contentsOfFile: path) else {
            print("error geting data from file")
            return []
        }
        let carsJSON = getDictionaryFromData(data)
        guard let arrayCars = carsJSON["cars"] as? [AnyObject] else {
            return []
        }
        var cars = [Car]()
        for resultsDict in arrayCars {
            if let result = resultsDict as? [String: AnyObject] {
                let car = parseCarFromDictionary(result)
                cars.append(car)
            }
        }
        return cars
    }
    
    static private func getDictionaryFromData(data: NSData) -> [String: AnyObject] {
        do {
            let json = try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String: AnyObject]
            return json!
        } catch let error {
            fatalError("\(error)")
        }
    }
    
    static private func parseCarFromDictionary(dictionary: [String: AnyObject]) -> Car {
        let car = Car()
        car.model = dictionary["model"] as! String
        car.manufacturer = dictionary["manufacturer"] as! String
        car.imageName = dictionary["imageName"] as! String
        car.engine = dictionary["engine"] as! String
        car.speed = dictionary["speed"] as! Int
        car.acceleration = dictionary["acceleration"] as! Int
        return car
    }
}