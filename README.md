# README #

Create an application what displays a list of cars with the following information:
-image of the car
-manufacturer
-model

By clicking on one of the car items, the application navigates to a new screen where detailed information of the car is displayed:
-image of the car
-manufacturer
-model
-engine size
-speed
-acceleration from 0-100